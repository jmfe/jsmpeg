#!/bin/sh

echo "正在编译 Javascript"


# Build the .wasm Module first
cat \
	src/jsmpeg.js \
	src/video-element.js \
	src/player.js \
	src/buffer.js \
	src/websocket.js \
	src/ts.js \
	src/decoder.js \
	src/mpeg1.js \
	src/mpeg1-wasm.js \
	src/mp2.js \
	src/mp2-wasm.js \
	src/webgl.js \
	src/canvas2d.js \
	src/webaudio.js \
	src/wasm-module.js \
	> jsmpeg.js

echo "JSMpeg.WASM_BINARY_INLINED='$(base64 -i jsmpeg.wasm)';" \
	>> jsmpeg.js

# build worker
cat \
  jsmpeg.js \
	worker.js \
	> jsmpeg.worker.js


# Minify
npx terser jsmpeg.worker.js -o jsmpeg.worker.min.js
npx terser jsmpeg.js -o jsmpeg.min.js