JSMpeg.Source.WebSocket = (function(){ "use strict";

var WSSource = function(url, options) {
	this.url = url;
	this.options = options;
	this.socket = null;
	this.streaming = true;
	this.chunks = []
	this.scheduling = false

	this.callbacks = {connect: [], data: []};
	this.destination = null;

	this.reconnectInterval = options.reconnectInterval !== undefined
		? options.reconnectInterval
		: 5;
	this.shouldAttemptReconnect = !!this.reconnectInterval;

	this.completed = false;
	this.established = false;
	this.progress = 0;

	this.reconnectTimeoutId = 0;

	this.onEstablishedCallback = options.onSourceEstablished;
	this.onCompletedCallback = options.onSourceCompleted; // Never used
};

WSSource.prototype.connect = function(destination) {
	this.destination = destination;
};

WSSource.prototype.destroy = function() {
	clearTimeout(this.reconnectTimeoutId);
	this.shouldAttemptReconnect = false;
	this.socket.close();
};

WSSource.prototype.start = function() {
	this.shouldAttemptReconnect = !!this.reconnectInterval;
	this.progress = 0;
	this.established = false;
	
	this.socket = new WebSocket(this.url, this.options.protocols || null);
	this.socket.binaryType = 'arraybuffer';
	this.socket.onmessage = this.onMessage.bind(this);
	this.socket.onopen = this.onOpen.bind(this);
	this.socket.onerror = this.onClose.bind(this);
	this.socket.onclose = this.onClose.bind(this);
};

WSSource.prototype.resume = function(secondsHeadroom) {
	// Nothing to do here
};

WSSource.prototype.onOpen = function() {
	this.progress = 1;
};

WSSource.prototype.onClose = function() {
	if (this.shouldAttemptReconnect) {
		clearTimeout(this.reconnectTimeoutId);
		this.reconnectTimeoutId = setTimeout(function(){
			this.start();	
		}.bind(this), this.reconnectInterval*1000);
	}
};

WSSource.prototype.shiftFrame = function() {
	if (!this.chunks.length) {
		this.scheduling = false
		return
	}

	var frame
	if (this.chunks.length > 10) {
		frame = this.chunks[this.chunks.length-1]
		this.chunks = []
	} else {
		frame = this.chunks.shift()
	}

	if (frame && this.destination) {
		this.destination.write(frame);
	}

	requestAnimationFrame(this.shiftFrame.bind(this))
};

WSSource.prototype.onMessage = function(ev) {
	var data = ev.data;
	if (typeof data === 'string') {
		// 命令
		if (this.options.onCommand) {
			this.options.onCommand(JSON.parse(data))
		}
		return;
	}

	var isFirstChunk = !this.established;
	this.established = true;

	if (isFirstChunk && this.onEstablishedCallback) {
		this.onEstablishedCallback(this);
	}

	this.chunks.push(ev.data)

	if (!this.scheduling) {
		this.scheduling = true
		this.shiftFrame()
	}
};

return WSSource;

})();

