const  YUVCanvas = require('yuv-canvas');
const video = document.getElementById('video');
const url = `ws://localhost:9999/ws/test`;

const yuv = YUVCanvas.attach(video)
const ws = new WebSocket(url);
ws.binaryType = 'arraybuffer';


const render = (buff) => {
  yuv.drawFrame(buff)
};

ws.onmessage = evt => {
  const data = evt.data;
  if (typeof data !== 'string') {
    render(new Uint8ClampedArray(data));
  } else {
    const payload = JSON.parse(data);
    switch (payload.type) {
      case 'initial':
        console.log('初始化画布');

        // update size
        break;
    }
  }
};
